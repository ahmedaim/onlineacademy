<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{

    /**
     * UsersController constructor.
     */
    public function __construct()    {
        // Trigger authentic users only
        $this->middleware('auth');
        parent::__construct();
    }


    /**
     * Get all Users
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {


        $limit = $request->limit?(int)$request->limit:12 ;
        $date_order = $request->date_order?$request->date_order:"desc" ;
        $users = User::query();
        $users = $users
            ->orderBy('created_at', $date_order)
            ->paginate($limit) ;

        return view('admin.users.index', compact('users'));
    }


    /**
     * Create form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        return view('admin.users.create'  );
    }


    /**
     * Insert new User
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        // Begin Validations
        $this->users_validation($request ,   "create");
        // End Validations





        // insert process

        $user = User::open($request->all())->save($request->all());

        if($user){
            // Process success
            Session::flash('success', "User created successfully");
        }else{
            // Process Fail
            Session::flash('error', "Sorry . inserting user process failed");
        }
        // success message

        // redirect back to same page
        return redirect()->back() ;
    }

    public function edit($id ){

        $get_user = User::find($id);

        if($get_user){
            return view('admin.users.update' , compact('get_user') );
        }else{

            // Process Fail
            Session::flash('error', "Sorry . this user does not exist");
            return redirect()->back();
        }


    }

    public function update($id , Request $request){

        // Begin Validations
        $this->users_validation($request ,   "update");
        // End Validations
        $user = User::find($id) ;


        if($user){
            // Process success
            $update_process =  $user->update($request->all());
            if($update_process){
                Session::flash('success', "User updated successfully");
            }else{
                Session::flash('error', "Sorry . updating user process failed");

            }

        }else{
            // Process Fail
            Session::flash('error', "Sorry . user not found");
        }
        return redirect()->back();
    }

    public function show($id , Request $request){



    }

    public function destroy($id , Request $request){
        $user = User::find($id) ;
        if($user){
            // Process success
            $delete_process =  $user->delete() ;
            if($delete_process){
                Session::flash('success', "User deleted successfully");
            }else{
                Session::flash('error', "Sorry . deleting  process failed");

            }

        }else{
            // Process Fail
            Session::flash('error', "Sorry . user not found");
        }
        return redirect()->back();
    }
    /**
     * Admin inputs validations
     * @param Request $request
     * @param $method
     */
    protected function users_validation(Request $request , $method  )
    {

        if($method == "update"){
            // validations for update process
            $this->validate($request, [
                'name' => 'required|between:2,150',
                'email' => 'required|email|max:255',
                'password' => 'required'
            ],
                [
                  'first_name.min' => 'First name must be at least 2 char',
                    'first_name.max' => 'First name maximum 150 char'
                ]
            );

        }else{
            // validations for insert process
            $this->validate($request, [
                    'name' => 'required|between:2,150',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required'
                ]
            );

        }


    }

    /**
     * Insert fake data for test process
     * @param Request $request
     */
    public function insert_fake_data(Request $request){

        // limit param for rows limitation
        $limit = $request->limit?(int)$request->limit:10 ;

        // initiate the process
//        $users = User::all();
//        if($users->count() < 50 ){
            factory(User::class, $limit)->create();
//        }


    }


}
