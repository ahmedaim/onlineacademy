@extends('admin/layout')

@section('styles')
    <link href="{{url('css/style.css')}}" rel="stylesheet" />
@stop

@section('content')

    <div class="col-md-10  ">

                <div class="row">
                    @include('partials._form-errors')
                    <div class="col-xs-10">
                        <div class="form-wrap">
                            <h1>Create new User</h1>
                            <form role="form" action="{{ action('UsersController@store') }}" method="post" autocomplete="off">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="sr-only">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name"  >
                                </div>
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email"  >
                                </div>
                                <div class="form-group">
                                    <label for="password" class="sr-only">Password</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" >
                                </div>


                                <input type="submit"   class="btn btn-custom btn-lg btn-block" value="Create">
                            </form>

                            <hr>
                        </div>
                    </div> <!-- /.col-xs-12 -->
                </div> <!-- /.row -->








    </div>

@stop

@section('javascripts')


@stop
