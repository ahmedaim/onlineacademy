@extends('admin/layout')

@section('styles')
    <link href="{{url('css/style.css')}}" rel="stylesheet" />
@stop

@section('content')

        <div class="col-md-10 content">
            @include('partials._form-errors')
            <div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-list"></span>User Lists
                        <div class="pull-right action-buttons">
                            <div class="btn-group pull-right">
                                <a href="{{url('admin/users/create')}}" class="btn btn-default btn-xs dropdown-toggle"  >
                                    <span class="glyphicon glyphicon-plus" style="margin-right: 0px;"></span>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(count($users) > 0)
                        <ul class="list-group">
                            @foreach($users as $user)
                                <li class="list-group-item">
                                    <div class="checkbox">

                                        <label >
                                            {{ $user->name }}
                                        </label>
                                        <label >
                                            {{ $user->email }}
                                        </label>
                                        <label >
                                            {{ $user->created_at->diffForHumans() }}
                                        </label>
                                    </div>
                                    <div class="pull-right action-buttons">
                                        <a href="{{ url('/admin/users/'.$user->id . '/edit') }}"><span class="glyphicon glyphicon-pencil"></span></a>
                                        <a onclick="return confirm('Are you sure  ?')" href="{{ url('/admin/users/delete/'.$user->id ) }}" class="trash"><span class="glyphicon glyphicon-trash"></span></a>
                                        </div>
                                </li>
                            @endforeach

                        </ul>

                        @else
                            <h1> No users found</h1>
                        @endif
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <h6>
                                    Page Count : <span class="label label-info">{{count($users)}}</span></h6>
                            </div>
                            <div class="col-md-6">
                                <ul class="pagination pagination-sm pull-right">
                                    {{ $users->links() }}

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

@stop

@section('javascripts')


@stop
